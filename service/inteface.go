package service

import "go-library/models"

type storage interface {
	CreateAuthor(author *models.Author) (*models.Author, error)
	CreateBook(book *models.Book) (*models.Book, error)
	GetAuthor(id int) (*models.Author, error)
	GetBook(id int) (*models.Book, error)
	GetAllAuthors() ([]*models.Author, error)
	GetAllBooks() ([]*models.Book, error)
	UpdateAuthor(author *models.Author) (*models.Author, error)
	UpdateBook(book *models.Book) (*models.Book, error)
	DeleteAuthor(id int) error
	DeleteBook(id int) error
}
