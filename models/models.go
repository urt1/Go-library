package models

type Author struct {
	ID    int    `json:"id" db:"id"`
	Name  string `json:"name" db:"name"`
	Email string `json:"email" db:"email"`
	Books []Book `json:"books" db:"books"`
}

type Book struct {
	ID       int     `json:"id" db:"id"`
	Name     string  `json:"name" db:"name"`
	Taken    bool    `json:"taken" db:"taken"`
	AuthorID *Author `json:"author_id" db:"author_id"`
}

type User struct {
	ID    int    `json:"id" db:"id"`
	Name  string `json:"name" db:"name"`
	Email string `json:"email" db:"email"`
	Books []Book `json:"books" db:"books"`
}
